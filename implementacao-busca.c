#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
    int horas;
    int minutos;
    int segundos;
} t_horario;

typedef struct {
    t_horario horario_partida;
    char *cidade_destino;
} voo;

typedef struct node {
    voo flight;
    struct node *next;
} node;

typedef struct {
    node *head;
    int tamanho;
} lista_voos;

int time_cmp(t_horario h1, t_horario h2);

lista_voos *criar_lista() {
    lista_voos *lista = (lista_voos *)malloc(sizeof(lista_voos));
    lista->head = NULL;
    lista->tamanho = 0;
    return lista;
}

void adicionar_voo(lista_voos *lista, t_horario partida, char *cidade_destino) {
    node *novo_voo = (node *)malloc(sizeof(node));
    novo_voo->flight.horario_partida = partida;
    novo_voo->flight.cidade_destino = strdup(cidade_destino);
    novo_voo->next = lista->head;
    lista->head = novo_voo;
    lista->tamanho++;
}

void imprimir_voo(voo flight) {
    printf("Horario de Partida: [%02d:%02d:%02d]\nDestino: %s\n",
           flight.horario_partida.horas, flight.horario_partida.minutos, flight.horario_partida.segundos,
           flight.cidade_destino);
}

void buscar_por_horario(lista_voos *lista, t_horario horario) {
    printf("Voos no Horario [%02d:%02d:%02d]:\n", horario.horas, horario.minutos, horario.segundos);

    node *atual = lista->head;
    while (atual != NULL) {
        if (time_cmp(atual->flight.horario_partida, horario) == 0) {
            imprimir_voo(atual->flight);
        }
        atual = atual->next;
    }
}

void buscar_entre_horarios(lista_voos *lista, t_horario inicio, t_horario fim) {
    printf("Voos entre [%02d:%02d:%02d] e [%02d:%02d:%02d]:\n", inicio.horas, inicio.minutos, inicio.segundos,
           fim.horas, fim.minutos, fim.segundos);

    node *atual = lista->head;
    while (atual != NULL) {
        if (time_cmp(atual->flight.horario_partida, inicio) >= 0 &&
            time_cmp(atual->flight.horario_partida, fim) <= 0) {
            imprimir_voo(atual->flight);
        }
        atual = atual->next;
    }
}

int time_cmp(t_horario h1, t_horario h2) {
    if (h1.horas != h2.horas) {
        return h1.horas - h2.horas;
    } else if (h1.minutos != h2.minutos) {
        return h1.minutos - h2.minutos;
    } else {
        return h1.segundos - h2.segundos;
    }
}

int main() {
    lista_voos *horarios = criar_lista();
    adicionar_voo(horarios, (t_horario){8, 0, 0}, "Chicago");
    adicionar_voo(horarios, (t_horario){10, 0, 0}, "Houston");
    adicionar_voo(horarios, (t_horario){12, 0, 0}, "Phoenix");
    adicionar_voo(horarios, (t_horario){14, 0, 0}, "Seatle");

    // Menu interativo
    int opcao;
    do {
        printf("\nMenu:\n");
        printf("1. Listar Todos os Voos\n");
        printf("2. Buscar por Horario Especifico\n");
        printf("3. Buscar Voos entre Horarios\n");
        printf("0. Sair\n");
        printf("Escolha uma opcao: ");
        scanf("%d", &opcao);

        switch (opcao) {
            case 1:
                printf("Lista de Todos os Voos:\n");
                node *atual_lista = horarios->head;
                while (atual_lista != NULL) {
                    imprimir_voo(atual_lista->flight);
                    atual_lista = atual_lista->next;
                }
                break;
            case 2:
                printf("Digite o horario (HH:MM:SS): ");
                t_horario horario;
                scanf("%d:%d:%d", &horario.horas, &horario.minutos, &horario.segundos);
                buscar_por_horario(horarios, horario);
                break;
            case 3:
                printf("Digite o horario de inicio (HH:MM:SS): ");
                t_horario inicio;
                scanf("%d:%d:%d", &inicio.horas, &inicio.minutos, &inicio.segundos);
                printf("Digite o horario de fim (HH:MM:SS): ");
                t_horario fim;
                scanf("%d:%d:%d", &fim.horas, &fim.minutos, &fim.segundos);
                buscar_entre_horarios(horarios, inicio, fim);
                break;
            case 0:
                printf("Saindo...\n");
                break;
            default:
                printf("Opcao inválida. Tente novamente.\n");
        }
    } while (opcao != 0);

    // Liberar memória
    node *temp;
    node *atual = horarios->head;
    while (atual != NULL) {
        temp = atual->next;
        free(atual->flight.cidade_destino);
        free(atual);
        atual = temp;
    }
    free(horarios);

    return 0;
}


